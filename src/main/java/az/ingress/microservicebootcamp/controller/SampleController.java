package az.ingress.microservicebootcamp.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Slf4j
@RequiredArgsConstructor
public class SampleController {

    private final Test test;
    private final Config config;

    @GetMapping("/{id}")
    public String sayHello(@PathVariable String id) {
        log.info("id is {}", id);
        log.info("name is {}", config.getName());
        log.info("students is {}", config.getStudents());
        log.info("count is {}", config.getCount());
        return "Hello";
    }

    @DeleteMapping
    public String sayHello1() {
        return "Hello";
    }

    @PostMapping
    public String sayHello2() {
        return "Hello";
    }

    @PutMapping
    public String sayHello3() {
        return "Hello";
    }

}
